package org.quovadis.streams.module7;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ComputeStatistics {

    public static void main (String[] args){
        Function<String, String> lineToName =
                line-> line.split(";")[1];

        Path path = Path.of("data/cities.csv");

        Set<String> cities = null;

        try(Stream<String> lines = Files.lines(path, StandardCharsets.ISO_8859_1)){

            cities = lines.skip(2)
                    .map(lineToName)
                    .collect(Collectors.toSet());

        }catch (IOException ioException){
            ioException.printStackTrace();
        }

        System.out.println("Cities = " + cities.size());

        List<String> citiesWithA = cities.stream()
                .filter(city-> city.startsWith("A"))
                .collect(Collectors.toList());

        System.out.println(citiesWithA);

        String[] arrayCities = cities.stream().toArray(String[]::new);

        System.out.println("# array = " + arrayCities.length);

        String joined = cities.stream()
                .filter(city -> city.length()==4)
                .collect(Collectors.joining(", ", "[", "]" ));

        System.out.println("Joined = " + joined);

        String collect = Stream.<String>of("one")
                .collect(Collectors.joining(", ", "[", "]" ));

        System.out.println("Collecting an empty stream: " + collect);
    }
}
