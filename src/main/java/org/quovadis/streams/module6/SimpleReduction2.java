package org.quovadis.streams.module6;

import java.util.List;

public class SimpleReduction2 {

    public static void main(String[] args){
        List<Integer> ints = List.of();

        int sum = ints.stream().reduce(10, (i1, i2)-> i1 + i2);

        System.out.println("Sum = " + sum);

    }
}
