package org.quovadis.streams.module6;

import java.util.List;
import java.util.Optional;

public class SimpleReduction {

    public static void main(String[] args){
        List<Integer> ints = List.of(1, 1, 1, 1 , 1);

        Optional<Integer> reduce =ints.stream().reduce((i1, i2)-> i1 + i2);

        System.out.println("reduce = " + reduce);

        reduce.get();
        Integer sum = reduce.orElseThrow();

        System.out.println("Sum = " + sum);
    }
}
