package org.quovadis.streams.module4;

public class StreamFromString {

    public static void main(String[] args){
        String sentence = "the quick brown fox jumps over the lazy dog";

        sentence.chars()
                .mapToObj(Character::toString)
                .filter(letter-> !letter.equals(" "))
                .distinct()
                .sorted()
                .forEach(System.out::print);
    }
}
